<?php

/**
 * @file
 * On behalf implementation of Feeds mapping API for prop.module.
 */

/**
 * Implements hook_feeds_processor_targets_alter().
 *
 * @see FeedsNodeProcessor::getMappingTargets().
 */
function _props_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  foreach (field_info_instances($entity_type, $bundle_name) as $name => $instance) {
    $info = field_info_field($name);
    if ($info['type'] == 'props') {
      foreach ($info['columns'] as $column_name => $column) {
        $targets[$name . ":" . $column_name] = array(
          'name' => $instance['label'] . ':' . $column_name,
          'callback' => 'props_set_target',
          'description' => t('@property for @name properties.', array('@property' => $column_name, '@name' => $instance['label'])),
          'real_target' => $name,
        );
      }
    }
  }
}

/**
 * Callback for mapping. Here is where the actual mapping happens.
 *
 * When the callback is invoked, $target contains the name of the field the
 * user has decided to map to and $value contains the value of the feed item
 * element the user has picked as a source.
 */
function props_set_target($source, $entity, $target, $value) {
  if (empty($value)) {
    return;
  }

  // Handle non-multiple value fields.
  if (!is_array($value)) {
    $value = array($value);
  }

  // Possible $sub_field values: label, value.
  list($field_name, $sub_field) = explode(':', $target);
  // Get field information.
  $info = field_info_field($field_name);
  // First time cleanup old data.
  if (isset($entity->{$field_name}[LANGUAGE_NONE][0])) {
    $field = $entity->$field_name;
  }
  else {
    $field = array();
  }

  // Iterate over all values.
  foreach ($value as $i => $v) {
    if (!is_array($v) && !is_object($v)) {
      $field[LANGUAGE_NONE][$i][$sub_field] = $v;
    }
    // Commented out because cardinality always unlimited.
    /*
    if ($info['cardinality'] == 1) {
      break;
    }*/
  }

  $entity->{$field_name} = $field;
}
